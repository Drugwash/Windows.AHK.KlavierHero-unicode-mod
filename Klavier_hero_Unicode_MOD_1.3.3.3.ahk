; http://ahkscript.org/germans/forums/viewtopic.php?t=6803
; Klavier Hero by Bentschi
; Modified version to support AHK v1.1 x32 Unicode and US keyboard
; Modified again by Drugwash to use scan codes and proper data types
; (best viewed with Tahoma 9pt font and tabstops set at 4 chars)

#NoEnv
#SingleInstance force
#WinActivateForce
SetTitleMatchMode, 2
SetBatchLines, -1
SetWinDelay, -1
SetControlDelay, -1
SetKeyDelay, -1, -1
SetMouseDelay, -1
ListLines, Off
SetFormat, Integer, D
SetWorkingDir, %A_ScriptDir%
DetectHiddenWindows, On
dbgT := A_TickCount
Debug		:= 0		; [boolean] Debug switch
AW			:= A_IsUnicode ? "W" : "A"
A_CharSize	:= A_IsUnicode ? 2 : 1
SWL			:= "user32\SetWindowLong" (A_PtrSize=8 ? "Ptr" : "") AW
SetVars		:= "MainX,MainY,MainW,MainH,KbdX,KbdY,KbdW,KbdH,PreX,PreY,PreW,PreH
					,DK,Instrument,Device,Vol,Vel,Octave,Channel,DrawMode,Layout,Preview,UseHints,Sweep"
IniFile			:= A_ScriptDir "\Klavier hero.ini"
Vol			:= 85		; [%] Initial volume
Vel			:= 50		; [%] Initial velocity
Device		:= 1		; 1=Default
DK				:= 1		; Drum kit
Instrument	:= 1		; [1-128] Default instrument
Channel		:= 1		; [1-16] Default channel
Octave		:= 1		; [-2-4] Default octave
MainX			:= 50		; [px] Default main window horizontal position
MainY		:= 50		; [px] Default main window vertical position
MainW		:= 500	; [px] Default main window width
MainH		:= 500	; [px] Default main window height
KbdX			:= 50		; [px] Default keyboard horizontal position
KbdY			:= 600	; [px] Default keyboard vertical position
KbdW		:= 600	; [px] Default keyboard width
KbdH			:= 170	; [px] Default keyboard height
PreX			:= 600	; [px] Default preview horizontal position
PreY			:= 50		; [px] Default preview vertical position
PreW			:= 500	; [px] Default preview width
PreH			:= 200	; [px] Default preview height
DrawMode	:= 1		; [0-1] Initial drawing mode (1=OpenGL)
Layout		:= ""		; Keyboard layout name in the list of all installed layouts
Preview		:= 1		; [boolean] Show layout preview window
UseHints		:= 1		; [boolean] Show characters on white keys
Sweep		:= 1		; [boolean] Drag mouse on keys for cascade notes
hintsFont	:= "Arial Unicode MS"

appname	:= "Klavier hero 2018"
version		:= "1.3.3.3"

EnvGet, isWine, WINEPREFIX		; find out if it's running under Wine
Menu, Tray, Tip, %appname% v%version%
Loop, Parse, SetVars, CSV
	IniRead, %A_LooPField%, %IniFile%, Settings, %A_LoopField%, % %A_LoopField%
Loop, 128
	{
	IniRead, i, %IniFile%, Instruments, %A_Index%, <???>
	Instruments .= A_Index A_Tab i "|"
	}
Loop, 9
	{
	IniRead, i, %IniFile%, Percussions, %A_Index%, <???>
	Percussions .= A_Index A_Tab i "|"
	}
Loop, 7
	IniRead, DevType%A_Index%, %IniFile%, DevTypes, %A_Index%, %A_Index%
StringTrimRight, Instruments, Instruments, 1
DK0				:= "0,8,16,24,25,32,40,48,56"
charR				:= 23		; number of white keys that can display characters (their scancodes below)
WKSC			:= "56,2C,2D,2E,2F,30,31,32,33,34,35,10,11,12,13,14,15,16,17,18,19,1A,1B"
HotkeysLine1	:= "56,1E,2C,1F,2D,20,2E,2F,22,30,23,31,32,25,33,26,34,27,35"
HotkeysLine2	:= "10,03,11,04,12,13,06,14,07,15,08,16,17,0A,18,0B,19,1A,0D,1B"
Hotkeys			:= HotkeysLine1 "," HotkeysLine2
Loop, Parse, Hotkeys, CSV
	{
	sc := "0x" A_Loopfield, sc+=0
	Key%sc% := A_Index, MKey%A_Index% := sc
	NumKeys := A_Index
	}
Loop, Parse, DK0, CSV
	DK%A_Index% := A_LoopField
DllCall("GetKeyboardLayoutName", "Str", currKLID:="00000000")
currHKL := DllCall("GetKeyboardLayout", "UInt", 0, "Ptr")
If !UseHints
	LayoutList := Layout
hWinMM := DllCall("kernel32\LoadLibrary" AW, "Str", "Winmm.dll", "Ptr")
NumDevs := DllCall("Winmm\midiOutGetNumDevs"), DCsz := 20+32*A_CharSize
Loop, % NumDevs
	{
	VarSetCapacity(DevCaps, DCsz, 0)
	DllCall("Winmm\midiOutGetDevCaps" AW, "UInt", A_Index-1, "Ptr", &DevCaps, "UInt", DCsz)
	Name := StrGet(&DevCaps+8)
	DevName%A_Index% := Name
	DevNames .= Name "|"
	}
StringTrimRight, DevNames, DevNames, 1
VarSetCapacity(DevCaps, 0), VarSetCapacity(Name, 0)
VarSetCapacity(HotkeysLine1, 0), VarSetCapacity(HotkeysLine2, 0)
Volume := Round(Vol*127/100)

Gui, Add, DDL, x-200 y-200 w0 h0 vFocusButton
Gui, Add, Text, x10 y10 cBlue vInstLabel, Instrument:
Gui, Add, ListBox, xp y+3 w200 h450 +0x80 +AltSubmit +hwndhInstList gChooseInstr vList,
VarSetCapacity(tabs, 4, 0), NumPut(16, tabs, 0, "UInt")
SendMessage, 0x192, 1, &tabs,, ahk_id %hInstList%	; LB_SETTABSTOPS
Gui, Add, Text, x+10 y10 cBlue, MIDI Device:
Gui, Add, DDL, xp y+3 w220 +AltSubmit Section vDev gChooseDev, % DevNames
Gui, Add, Text, xp y+5 w50 cBlue, Type:
Gui, Add, Text, x+1 yp w170 vDeviceT,
Gui, Add, Text, xs y+5 w50 cBlue, ID:
Gui, Add, Text, x+1 yp w170 vDeviceI,
Gui, Add, Text, xs y+5 cBlue, Channels:
Gui, Add, Text, x+1 yp w25 vDeviceC,
Gui, Add, Text, x+5 yp cBlue, Voices:
Gui, Add, Text, x+1 yp w25 vDeviceV,
Gui, Add, Text, x+5 yp cBlue, Notes:
Gui, Add, Text, x+1 yp w25 vDeviceN,

Gui, Add, Text, xs y+15 w50 cBlue vVolLabel, Volume:
Gui, Add, Text, x+1 yp w35 vVolVal, %Vol% `%
Gui, Add, Slider, xs y+3 w220 hwndhVol vVol gChooseVol +0x100 AltSubmit Range0-100 TickInterval25
	, % Vol
Gui, Add, Text, xs y+15 w50 cBlue vVelLabel, Velocity:
Gui, Add, Text, x+1 yp w35 vVelVal, %Vel% `%
Gui, Add, Slider, xs y+3 w220 hwndhVel vVel gChooseVel +0x100 AltSubmit Range0-100 TickInterval25
	, % Vel
Gui, Add, Text, xp y+10 cBlue Section, Octave:
Gui, Add, Edit, x+2 yp-4 w40
Gui, Add, UpDown, Range-2-4 vOctave gChooseOctave, % Octave
Gui, Add, Text, x+10 ys cBlue, Channel:
Gui, Add, DDL, x+2 yp-4 w85 +Altsubmit vChannel gChooseChannel
	, 1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16
GuiControl, Choose, Dev, % Device
GuiControlGet, ch, Pos, Channel
Gui, Add, Text, xs y+15 cBlue, Virtual keyboard display:
Gui, Add, DDL, x%chX% yp-4 w85 +AltSubmit vDrawMode gChooseDrawMode, [Hidden]|OpenGL
GuiControl, Choose, DrawMode, % DrawMode+1
Gui, Add, CheckBox, xs y+10 Checked%UseHints% vUseHints gToggleHints, Show hints on keys
Gui, Add, CheckBox, xp y+10 Checked%Preview% vPreview gTogglePreview
	, Show preview when switching layouts
Gui, Add, Button, x+1 yp-4 h21 vBtnPreview gShowPreview, >
Gui, Add, Text, xs y+6 cBlue vLayoutLabel, Physical keyboard layout:
Gui, Add, DDL, xp y+3 w220 R30 vLayout gChooseLayout, % LayoutList
GuiControl, ChooseString, Layout, % Layout
Gui, Add, CheckBox, xp y+10 Checked%Sweep% vSweep gToggleSweep
	, Allow sweep effect on mouse drag
Gui, +LastFound +hwndhMain
Gui, Show, Hide w%MainW% h%MainH%, %appname% v%version%
SetWndPos(hMain, MainX, MainY, MainW, MainH)

hWP1 := RegisterCallback("WndProc", "F", 4, 0)
midiOutShortMsg := DllCall("kernel32\GetProcAddress", "Ptr", hWinMM, "AStr", "midiOutShortMsg", "Ptr")
GoSub, ChooseDev
Gui, Show, AutoSize
OnExit, Cleanup
OnMessage(0x100, "PlayTone", 10)		; WM_KEYDOWN
OnMessage(0x101, "ReleaseTone", 10)	; WM_KEYUP
OnMessage(0x201, "MouseClick")			; WM_LBUTTONDOWN
OnMessage(0x203, "MouseClick")			; WM_LBUTTONDBLCLK
OnMessage(0x202, "MouseRelease")		; WM_LBUTTONUP
OnMessage(0x20A, "MWheel")				; WM_MOUSEWHEEL
OnMessage(0xA4, "HideKeys")				; WM_NCRBUTTONDOWN
If Sweep
	OnMessage(0x200, "MouseMove")		; WM_MOUSEMOVE
GoSub, EnableDraw
GoSub ToggleHints
MainInit := 1
If Debug
	OutputDebug, % "Total load time:" A_TickCount-dbgT
Return
;================================================================
Cleanup:
Critical
� := A_TickCount
GetWndPos(hMain, MainX, MainY, MainW, MainH)
GetWndPos(hPreview, PreX, PreY, PreW, PreH)
GoSub, DisableDraw
If hKeys
	{
	DllCall(glDeleteLists, "UInt", FontOut, "UInt", charR)
	DllCall(glDeleteLists, "UInt", FontIn, "UInt", charR)
	DllCall("opengl32\wglMakeCurrent", "Ptr", 0, "Ptr", 0)
	DllCall("opengl32\wglDeleteContext", "Ptr", hRC)
	DllCall("gdi32\SelectObject", "Ptr", hDC, "Ptr", hOFont)
	DllCall("user32\ReleaseDC", "Ptr", hKeys, "Ptr", hDC)
	DllCall("gdi32\DeleteObject", "Ptr", hFont)
	DllCall("kernel32\FreeLibrary", "Ptr", hOpenGL)
	DllCall("kernel32\FreeLibrary", "Ptr", hGdi32)
	DllCall(SWL
		, "Ptr", hKeys
		, "Int", -4	; GWL_WNDPROC
		, "Ptr", hWP
		, "Ptr")
	Gui, 2:Destroy
	}
GoSub, FreeMidi
DllCall("kernel32\FreeLibrary", "Ptr", hWinMM)
Loop, Parse, SetVars, CSV
	IniWrite, % %A_LooPField%, %IniFile%, Settings, %A_LoopField%
If Debug
	OutputDebug, % A_ThisLabel ": " A_TickCount-�
GuiClose:
ExitApp
Reload:
Reload
;================================================================
InitMidi:
If (Device > NumDevs)
  Device := 1
If (hMidiOut)
  GoSub, FreeMidi
VarSetCapacity(hMidiOut, A_PtrSize, 0)
DllCall("Winmm\midiOutOpen", "PtrP", hMidiOut, "UInt", Device-1, "Ptr", 0, "Ptr", 0, "UInt", 0)
Return
;================================================================
FreeMidi:
If (PDevVol != "")
	DllCall("Winmm\midiOutSetVolume", "Ptr", hMidiOut, "UInt", PDevVol) ; restore device volume
DllCall(midiOutShortMsg, "Ptr", hMidiOut, "UInt", 0xFF)	; Full device reset
DllCall("Winmm\midiOutReset", "Ptr", hMidiOut)
DllCall("Winmm\midiOutClose", "Ptr", hMidiOut)
Return
;================================================================
ChooseDev:
GuiControlGet, Device,, Dev
VarSetCapacity(DevCaps, DCsz, 0)
DllCall("Winmm\midiOutGetDevCaps" AW, "UInt", Device-1, "Ptr", &DevCaps, "UInt", DCsz)
i1 := Format("{:04x}", NumGet(DevCaps, 0, "UShort"))
i2 := Format("{:04x}", NumGet(DevCaps, 2, "UShort"))
DevID		:= "MID_" i1 "   PID_" i2
DevType	:= NumGet(DevCaps, 8+32*A_CharSize, "UShort")
DevVoices	:= NumGet(DevCaps, 10+32*A_CharSize, "UShort")
DevNotes	:= NumGet(DevCaps, 12+32*A_CharSize, "UShort")
DevChan	:= NumGet(DevCaps, 14+32*A_CharSize, "UShort")
DevChan	:= DevChan != 0xFFFF ? DevChan : 16
DevOpt		:= NumGet(DevCaps, 16+32*A_CharSize, "UInt")
GuiControl,, DeviceT, % DevType%DevType%
GuiControl,, DeviceV, %DevVoices%
GuiControl,, DeviceN, %DevNotes%
GuiControl,, DeviceC, %DevChan%
GuiControl,, DeviceI, %DevID%
NoVol := !(DevOpt & 0x1)	; MIDICAPS_VOLUME
if !isWine
	{
	GuiControl, % (NoVol ? "Disable" : "Enable"), VolLabel
	GuiControl, % (NoVol ? "Disable" : "Enable"), VolVal
	GuiControl, % (NoVol ? "Disable" : "Enable"), Vol	; in Wine there's no volume - 2020.12.12
	}
; if volume available, get current values before any changes and restore them on device close
If !NoVol
	DllCall("Winmm\midiOutGetVolume", "Ptr", hMidiOut, "UIntP", PDevVol)
Else PDevVol := ""
GoSub InitMidi
GuiControl,, Channel, |
Loop, % DevChan
	If A_Index!=10
		GuiControl,, Channel, %A_Index% (generic)
	Else GuiControl,, Channel, 10 (percuss.)
Channel := Channel > DevChan ? 1 : Channel
GuiControl, Choose, Channel, % Channel
GoSub ChooseVol
GoSub ChooseChannel
GoSub ChooseVel
Return
;================================================================
ChooseVol:
GuiControlGet, Vol
Volume := Round(Vol*0xFFFF/100)
GuiControl,, VolVal, %Vol% `%
DllCall("Winmm\midiOutSetVolume", "Ptr", hMidiOut, "UInt", (Volume | Volume << 16)) ; device volume
Return
;================================================================
ChooseVel:
GuiControlGet, Vel
Velocity := Round(Vel*127/100)
GuiControl,, VelVal, %Vel% `%
Return
;================================================================
ChooseChannel:
GuiControlGet, Channel
If (Channel=LastChannel)
	GoTo ChooseInstr
NoteOn := 143+Channel, NoteOff := 127+Channel, CtrlChg := 175+Channel, PrgChg := 191+Channel
DllCall(midiOutShortMsg, "Ptr", hMidiOut, "UInt", (0x79<<8) + CtrlChg)
If Channel=10		; Percussions only
	{
	GuiControl,, InstLabel, Drum kits:
	If !oInstrument
		oInstrument := Instrument
	Instrument := DK	; 1,9,17,25,26,33,41,49,57
	GuiControl,, List, |%Percussions%
	}
Else
	{
	GuiControl,, InstLabel, Instruments:
	If (!LastChannel OR LastChannel=10)	; only refresh the list when necessary
		GuiControl,, List, |%Instruments%
	If oInstrument
		{
		DK := Instrument, Instrument := oInstrument, oInstrument := 0
		}
	}
GuiControl, Choose, List, %Instrument%
GoSub ChooseInstr
LastChannel := Channel
Return
;================================================================
ChooseInstr:
GuiControlGet, Instrument,, List
i := Instrument-1
If Channel=10
	{
	 i := DK%Instrument%, DK := Instrument
	}
DllCall(midiOutShortMsg, "Ptr", hMidiOut, "UInt", (i<<8) + PrgChg)
Return
;================================================================
ChooseOctave:
GuiControlGet, Octave
Return
;================================================================
ChooseLayout:
Critical
GuiControlGet, Layout
KLID := GetSelKLID(listHKL, Layout)
GuiControl, 1:Choose, DrawMode, 2
DrawMode:=1
GoTo EnableDraw
Return
;================================================================
ChooseDrawMode:
GuiControlGet, DrawMode
DrawMode--

EnableDraw:
If !DrawMode
	GoTo DisableDraw
If !hKeys
	{
	Gui, 2:+ToolWindow -SysMenu +E0x40000 +Resize +MinSize200x50 -DPIscale +hwndhKeys
	hWP := DllCall(SWL
		, "Ptr", hKeys
		, "Int", -4	; GWL_WNDPROC
		, "Ptr", hWP1
		, "Ptr")
	If DrawMode=1
	  GoTo, InitOpenGL
	}
Else If (Layout != LastLayout && UseHints && HintsInit)
	{
	LastLayout := Layout
	DllCall(glDeleteLists, "UInt", FontOut, "UInt", charR)
	DllCall(glDeleteLists, "UInt", FontIn, "UInt", charR)
	GoTo UpdateLayout
	}
Else Gui, 2:Show
Return
;================================================================
2GuiClose:
GuiControl, 1:Choose, DrawMode, 1
DrawMode--

DisableDraw:
Critical
GetWndPos(hKeys, KbdX, KbdY, KbdW, KbdH)
Gui, 2:Hide
Return
;================================================================
2GuiSize:
If !MainInit
	Return
;KbdCW := A_GuiWidth, KbdCH := A_GuiHeight
GetClientSz(hKeys, KbdCW, KbdCH)
DllCall("user32\RedrawWindow", "Ptr", hKeys, "Ptr", 0, "Ptr", 0, "UInt", 0x143)
; RDW_UPDATENOW=0x100 ; RDW_NOCHILDREN=0x40
; RDW_INTERNALPAINT=0x2 ; RDW_INVALIDATE=0x1
SetTimer, GetWndPosLabel, -100
Return
;================================================================
GetWndPosLabel:
GetWndPos(hKeys, KbdX, KbdY, KbdW, KbdH)
GetClientSz(hKeys, KbdCW, KbdCH)
Return
;================================================================
TogglePreview:
GuiControlGet, Preview
If (Preview && UseHints)
	FillDemoKbd(HKL)
Else Gui, 3:Hide
Return
;================================================================
ShowPreview:
DetectHiddenWindows, Off	; because of GetWindowPlacement() - see end of script
If !WinExist("ahk_id " hPreview)
	FillDemoKbd(HKL)
Else Gui, 3:Hide
DetectHiddenWindows, On
Return
;================================================================
ToggleHints:
GuiControlGet, UseHints
GuiControl, % (UseHints ? "Enable" : "Disable"), LayoutLabel
GuiControl, % (UseHints ? "Enable" : "Disable"), Layout
GuiControl, % (UseHints ? "Enable" : "Disable"), Preview
GuiControl, % (UseHints ? "Enable" : "Disable"), BtnPreview
If (UseHints && !HintsInit)
	GoSub InitHints
Else
	{
	GoSub TogglePreview
	Goto UpdateOpenGL
	}
Return
;================================================================
ToggleSweep:
GuiControlGet, Sweep
OnMessage(0x200, Sweep ? "MouseMove" : "")
Return
;================================================================
InitOpenGL:
� := A_TickCount
hOpenGL		:= DllCall("kernel32\LoadLibrary" AW, "Str", "opengl32.dll", "Ptr")
hGdi32			:= DllCall("kernel32\LoadLibrary" AW, "Str", "gdi32.dll", "Ptr")
hDC				:= DllCall("user32\GetDC", "Ptr", hKeys, "Ptr")
VarSetCapacity(pfd, 40, 0)			; PIXELFORMATDESCRIPTOR struct
NumPut(40		, pfd, 0, "UShort")	; nSize
NumPut(1		, pfd, 2, "UShort")	; nVersion
NumPut(0x25	, pfd, 4, "UInt")		; PFD_SUPPORT_OPENGL/DRAW_TO_WINDOW/DOUBLEBUFFER
NumPut(0		, pfd, 8, "UChar")	; iPixelType PFD_TYPE_RGBA
NumPut(32		, pfd, 9, "UChar")	; cColorBits
NumPut(16		, pfd, 23, "UChar")	; cDepthBits
NumPut(0		, pfd, 26, "UChar")	; iLayerType
PixelFormat		:= DllCall("gdi32\ChoosePixelFormat", "Ptr", hDC, "Ptr", &pfd)
DllCall("gdi32\SetPixelFormat", "Ptr", hDC, "UInt", PixelFormat, "Ptr", &pfd)
hRC				:= DllCall("opengl32\wglCreateContext", "Ptr", hDC, "Ptr")
DllCall("opengl32\wglMakeCurrent", "Ptr", hDC, "Ptr", hRC)
SwapBuffers	:= DllCall("kernel32\GetProcAddress", "Ptr", hGdi32   , "AStr", "SwapBuffers"	, "Ptr")
glClear			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glClear"			, "Ptr")
glViewport		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glViewport"		, "Ptr")
glMatrixMode	:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glMatrixMode"	, "Ptr")
glLoadIdentity	:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glLoadIdentity", "Ptr")
glOrtho			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glOrtho"			, "Ptr")
glColor4f			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glColor4f"		, "Ptr")
glRotatef		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glRotatef"		, "Ptr")
glScalef			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glScalef"			, "Ptr")
glTranslatef		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glTranslatef"	, "Ptr")
glVertex2f		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glVertex2f"		, "Ptr")
glPushMatrix	:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glPushMatrix"	, "Ptr")
glPopMatrix		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glPopMatrix"		, "Ptr")
glEnable			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glEnable"			, "Ptr")
glDisable			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glDisable"			, "Ptr")
glBegin			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glBegin"			, "Ptr")
glEnd				:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glEnd"				, "Ptr")
glGenLists		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glGenLists"		, "Ptr")
glDeleteLists	:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glDeleteLists"	, "Ptr")
glNewList		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glNewList"		, "Ptr")
glCallList			:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glCallList"			, "Ptr")
glEndList		:= DllCall("kernel32\GetProcAddress", "Ptr", hOpenGL, "AStr", "glEndList"		, "Ptr")

GL_BLEND								:= 0x0BE2
GL_COLOR_BUFFER_BIT			:= 0x00004000
GL_COMPILE							:= 0x1300
GL_LINE_LOOP						:= 0x0002
GL_LINE_SMOOTH					:= 0x0B20
GL_MODELVIEW						:= 0x1700
GL_ONE_MINUS_SRC_ALPHA	:= 0x0303
GL_PROJECTION						:= 0x1701
GL_QUADS								:= 0x0007
GL_SRC_ALPHA						:= 0x0302
GL_TRIANGLES						:= 0x0004

DllCall("opengl32\glBlendFunc", "UInt", GL_SRC_ALPHA, "UInt", GL_ONE_MINUS_SRC_ALPHA)

NumWKeys := 0, Index := 1
Loop, % NumKeys
	{
	If Index in 1,3,5,7,8,10,12
		NumWKeys++
	Index++
	If Index > 12
		Index := 1
	}
KeyWW := 100/NumWKeys, KeyBW := KeyWW/4*2.5, KeyC := 5
KeyBW2 := KeyBW/2, KeyBW4 := KeyBW/4, KeyW8 := KeyWW*0.8
MYM := 10/4*2.5

List := DllCall(glGenLists, "UInt", 6)
DllCall(glNewList, "UInt", List+0, "UInt", GL_COMPILE)
DllCall(glLoadIdentity)
DllCall(glColor4f, "Float", 0, "Float", 0, "Float", 0, "Float", 1)
DllCall(glBegin, "UInt", GL_LINE_LOOP)
DllCall(glVertex2f, "Float", 0.001, "Float", 0.001)
DllCall(glVertex2f, "Float", 0.001, "Float", 9.999)
DllCall(glVertex2f, "Float", 99.999, "Float", 9.999)
DllCall(glVertex2f, "Float", 99.999, "Float", 0.001)
DllCall(glEnd)
DllCall(glEndList)

DllCall(glNewList, "UInt", List+1, "UInt", GL_COMPILE)
DllCall(glBegin, "UInt", GL_QUADS)
DllCall(glColor4f, "Float", 0.5, "Float", 0.5, "Float", 0.5, "Float", 1)
DllCall(glVertex2f, "Float", 0, "Float", 0)
DllCall(glVertex2f, "Float", KeyWW, "Float", 0)
DllCall(glColor4f, "Float", 1, "Float", 1, "Float", 1, "Float", 1)
DllCall(glVertex2f, "Float", KeyWW, "Float", 1)
DllCall(glVertex2f, "Float", 0, "Float", 1)
DllCall(glVertex2f, "Float", 0, "Float", 1)
DllCall(glVertex2f, "Float", KeyWW, "Float", 1)
DllCall(glVertex2f, "Float", KeyWW, "Float", 9.6)
DllCall(glVertex2f, "Float", 0, "Float", 9.6)
DllCall(glEnd)
DllCall(glColor4f, "Float", 0, "Float", 0, "Float", 0, "Float", 1)
DllCall(glBegin, "UInt", GL_LINE_LOOP)
DllCall(glVertex2f, "Float", 0, "Float", 0)
DllCall(glVertex2f, "Float", KeyWW, "Float", 0)
DllCall(glVertex2f, "Float", KeyWW, "Float", 9.6)
DllCall(glVertex2f, "Float", 0, "Float", 9.6)
DllCall(glEnd)
DllCall(glTranslatef, "Float", KeyWW, "Float", 0, "Float", 0)
DllCall(glEndList)

DllCall(glNewList, "UInt", List+2, "UInt", GL_COMPILE)
DllCall(glTranslatef, "Float", KeyWW, "Float", 0, "Float", 0)
DllCall(glBegin, "UInt", GL_QUADS)
DllCall(glColor4f, "Float", 0.15, "Float", 0.15, "Float", 0.15, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 0)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 0)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 10/4*2.5)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 10/4*2.5)
DllCall(glColor4f, "Float", 0.2, "Float", 0.2, "Float", 0.2, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 0.5)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 0.5)
DllCall(glColor4f, "Float", 0.5, "Float", 0.5, "Float", 0.5, "Float", 1)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 10/4*2.0)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 10/4*2.0)
DllCall(glColor4f, "Float", 0.2, "Float", 0.2, "Float", 0.2, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 0.5)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 0.5)
DllCall(glColor4f, "Float", 0.5, "Float", 0.5, "Float", 0.5, "Float", 1)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 0)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 0)
DllCall(glColor4f, "Float", 0.3, "Float", 0.3, "Float", 0.3, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 10/4*2.0)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 10/4*2.0)
DllCall(glColor4f, "Float", 0.2, "Float", 0.2, "Float", 0.2, "Float", 1)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 10/4*2.5)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 10/4*2.5)
DllCall(glEnd)
DllCall(glEndList)

DllCall(glNewList, "UInt", List+3, "UInt", GL_COMPILE)
DllCall(glBegin, "UInt", GL_QUADS)
DllCall(glColor4f, "Float", 0.4, "Float", 0.4, "Float", 0.4, "Float", 1)
DllCall(glVertex2f, "Float", 0, "Float", 0)
DllCall(glVertex2f, "Float", KeyWW, "Float", 0)
DllCall(glColor4f, "Float", 0.9, "Float", 0.9, "Float", 0.9, "Float", 1)
DllCall(glVertex2f, "Float", KeyWW, "Float", 1.5)
DllCall(glVertex2f, "Float", 0, "Float", 1.5)
DllCall(glVertex2f, "Float", 0, "Float", 1.5)
DllCall(glVertex2f, "Float", KeyWW, "Float", 1.5)
DllCall(glColor4f, "Float", 0.7, "Float", 0.7, "Float", 0.7, "Float", 1)
DllCall(glVertex2f, "Float", KeyWW, "Float", 9.6)
DllCall(glVertex2f, "Float", 0, "Float", 9.6)
DllCall(glVertex2f, "Float", 0, "Float", 9.6)
DllCall(glVertex2f, "Float", KeyWW, "Float", 9.6)
DllCall(glVertex2f, "Float", KeyWW-0.4, "Float", 9.7)
DllCall(glVertex2f, "Float", 0.4, "Float", 9.7)
DllCall(glEnd)
DllCall(glColor4f, "Float", 0, "Float", 0, "Float", 0, "Float", 1)
DllCall(glBegin, "UInt", GL_LINE_LOOP)
DllCall(glVertex2f, "Float", 0, "Float", 0)
DllCall(glVertex2f, "Float", KeyWW, "Float", 0)
DllCall(glVertex2f, "Float", KeyWW, "Float", 9.6)
DllCall(glVertex2f, "Float", KeyWW-0.4, "Float", 9.7)
DllCall(glVertex2f, "Float", 0.4, "Float", 9.7)
DllCall(glVertex2f, "Float", 0, "Float", 9.6)
DllCall(glEnd)
DllCall(glTranslatef, "Float", KeyWW, "Float", 0, "Float", 0)
DllCall(glEndList)

DllCall(glNewList, "UInt", List+4, "UInt", GL_COMPILE)
DllCall(glTranslatef, "Float", KeyWW, "Float", 0, "Float", 0)
DllCall(glBegin, "UInt", GL_QUADS)
DllCall(glColor4f, "Float", 0, "Float", 0, "Float", 0, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 0)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 0)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 10/4*2.5)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 10/4*2.5)
DllCall(glColor4f, "Float", 0.1, "Float", 0.1, "Float", 0.1, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 0.5)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 0.5)
DllCall(glColor4f, "Float", 0.25, "Float", 0.25, "Float", 0.25, "Float", 1)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 10/4*2.0)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 10/4*2.0)
DllCall(glColor4f, "Float", 0.1, "Float", 0.1, "Float", 0.1, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 0.5)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 0.5)
DllCall(glColor4f, "Float", 0.25, "Float", 0.25, "Float", 0.25, "Float", 1)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 0)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 0)
DllCall(glColor4f, "Float", 0.15, "Float", 0.15, "Float", 0.15, "Float", 1)
DllCall(glVertex2f, "Float", -KeyBW4, "Float", 10/4*2.0)
DllCall(glVertex2f, "Float", KeyBW4, "Float", 10/4*2.0)
DllCall(glColor4f, "Float", 0.1, "Float", 0.1, "Float", 0.1, "Float", 1)
DllCall(glVertex2f, "Float", KeyBW2, "Float", 10/4*2.5)
DllCall(glVertex2f, "Float", -KeyBW2, "Float", 10/4*2.5)
DllCall(glEnd)
DllCall(glEndList)

PointSize := 1
DllCall(glNewList, "UInt", List+5, "UInt", GL_COMPILE)
DllCall(glEnable, "UInt", GL_BLEND)
DllCall(glBegin, "UInt", GL_TRIANGLES)
DllCall(glColor4f, "Float", 1, "Float", 0, "Float", 0, "Float", 1)
DllCall(glVertex2f, "Float", 0, "Float", 0)
DllCall(glColor4f, "Float", 1, "Float", 0, "Float", 0, "Float", 0)
DllCall(glVertex2f, "Float", 0, "Float", PointSize)
DllCall(glVertex2f, "Float", Sin(10.0531)*PointSize, "Float", -(Cos(10.0531)*PointSize))
DllCall(glEnd)
DllCall(glDisable, "UInt", GL_BLEND)
DllCall(glEndList)
If Debug
	OutputDebug, % A_ThisLabel ": " A_TickCount-�
;================================================================
UpdateLayout:
Critical
�1 := A_TickCount
If (!UseHints OR !HintsInit)
	GoTo SkipHints
FontOut := DllCall(glGenLists, "UInt", charR)
FontIn := DllCall(glGenLists, "UInt", charR)
HKL := DllCall("user32\LoadKeyboardLayout" AW
	, "Str", KLID
	, "UInt", 0x81 ; KLF_NOTELLSHELL/ACTIVATE	; KLF_SUBSTITUTE_OK is not good here !
	, "Ptr")
VarSetCapacity(gmf, charR*24, 0)	; GLYPHMETRICSFLOAT struct
Loop, Parse, WKSC, CSV
	{
	i := A_Index-1
	If !CC := Ord(GetChar("0x" A_LoopField, HKL))
		CC := 32	; If no char can be found, use code for 'SPACE' to draw a blank rather than square
	DllCall("opengl32\wglUseFontOutlines" AW
		, "Ptr"  , hDC			; hDC
		, "UInt", CC			; first
		, "UInt", 1				; count
		, "UInt", FontOut+i	; listBase
		, "Float", 0				; deviation
		, "Float", 0.2			; extrusion
		, "Int"  , 0				; format [WGL_FONT_LINES]
		, "Ptr"  , &gmf)		; lpgmf
	DllCall("opengl32\wglUseFontOutlines" AW
		, "Ptr"  , hDC
		, "UInt", CC
		, "UInt", 1
		, "UInt", FontIn+i
		, "Float", 0
		, "Float", 0.2
		, "Int"  , 1				; format [WGL_FONT_POLYGONS]
		, "Ptr"  , 0)
	}
If (Preview && UseHints)
	FillDemoKbd(HKL)
If HKL not in %instHKL%
	DllCall("user32\UnloadKeyboardLayout", "Ptr", HKL)
DllCall("ActivateKeyboardLayout", "Ptr", currHKL, "UInt", 0)
SkipHints:
; TODO: find current monitor, compare X and Y against current monitor's size, adjust X Y as necessary
If !DrawInit ; This must be done to avoid Windows vs Client size mismatch (window would shrink)
	{
	Gui, 2:Show, Hide w%KbdW% h%KbdH%, Virtual keyboard
	SetWndPos(hKeys, KbdX, KbdY, KbdW, KbdH, 0)
	}
Gui, 2:Show
GetClientSz(hKeys, KbdCW, KbdCH)
DrawInit := 1
If Debug
	OutputDebug, % "UpdateLayout: " A_TickCount-�1 " (UseHints=" UseHints ", HintsInit=" HintsInit ") Label=" A_ThisLabel
;================================================================
UpdateOpenGL:
Critical
�1 := A_TickCount
DllCall(glClear, "UInt", 0x4000)					; GL_COLOR_BUFFER_BIT
DllCall(glViewport, "Int", 0, "Int", 0, "Int", KbdCW, "Int", KbdCH)
DllCall(glMatrixMode, "UInt", 0x1701)			; GL_PROJECTION
DllCall(glLoadIdentity)
DllCall(glOrtho, "Double", 0, "Double", 100, "Double", 10, "Double", 0, "Double", -1, "Double", 1)
DllCall(glMatrixMode, "UInt", 0x1700)			; GL_MODELVIEW
DllCall(glLoadIdentity)
Index := 1, cOctave := 1, co := 0
Loop, % NumKeys
	{
	If Index in 1,3,5,7,8,10,12	; White keys
		{
		DllCall(glCallList, "UInt", ((KeyI%A_Index%down) ? List+3 : List+1))
		DllCall(glPushMatrix)
		DllCall(glTranslatef, "Float", -KeyW8, "Float", 9, "Float", 0)
		DllCall(glScalef, "Float", 3, "Float", -3, "Float",  1)
		If UseHints
			{
			DllCall(glPushMatrix)
			If (Index = 8 && cOctave = Octave)
				DllCall(glColor4f, "Float", 0.6, "Float", 0.6, "Float", 0.6, "Float", 0.9)		; RGBA
			Else DllCall(glColor4f, "Float", 0.6, "Float", 0.6, "Float", 0.6, "Float", 0.9)	; RGBA
			DllCall(glEnable, "UInt", 0x0BE2)		; GL_BLEND
			DllCall(glEnable, "UInt", 0x0B20)		; GL_LINE_SMOOTH
			DllCall(glCallList, "UInt", FontOut+co)
			DllCall(glDisable, "UInt", 0x0B20)	; GL_LINE_SMOOTH
			DllCall(glDisable, "UInt", 0x0BE2)	; GL_BLEND
			DllCall(glPopMatrix)
			If (Index = 8 && cOctave = Octave)
				DllCall(glColor4f, "Float", 1, "Float", 0.3, "Float", 0.8, "Float", 1)		; RGBA
			Else DllCall(glColor4f, "Float", 0.8, "Float", 0.8, "Float", 0.8, "Float", 1)	; RGBA
			DllCall(glCallList, "UInt", FontIn+co)
			}
		DllCall(glPopMatrix)
		co++
		}
	Index++
	if Index > 12
		{
		Index := 1
		cOctave--
		}
	}
DllCall(glLoadIdentity)
Index := 1
Loop, % NumKeys
	{
	If Index in 2,4,6,9,11	; Black keys
		DllCall(glCallList, "UInt", ((KeyI%A_Index%down) ? List+4 : List+2))
	If Index in 6,11
		DllCall(glTranslatef, "Float", KeyWW, "Float", 0, "Float", 0)
	Index++
	If Index > 12
		Index := 1
	}
DllCall(SwapBuffers, "Ptr", hDC)
If Debug
	OutputDebug, % "UpdateOpenGL: " A_TickCount-�1 " (UseHints=" UseHints ", HintsInit=" HintsInit ") Label=" A_ThisLabel
Return
;================================================================
InitHints:
Critical
� := A_TickCount
LayoutList := LayoutList(listHKL)
If Layout in ,ERROR
	Layout := currName
If Layout is integer
	Layout := currName
KLID := GetSelKLID(listHKL, Layout), BuildDemoKbd(currHKL)
;Gui, 3:Font, %hintsFont%	; Wine is shit at toolwindow title font
Gui, 3:Show, Hide w%PreW% h%PreH%, Preview layout: %Layout%
SetWndPos(hPreview, PreX, PreY, PreW, PreH, 0)
Gui, 3:Show, Hide AutoSize
hFont := DllCall("gdi32\CreateFont" AW
	, "Int"  , -1		; nHeight
	, "Int"  , 0		; nWidth
	, "Int"  , 0		; nEscapement
	, "Int"  , 0		; nOrientation
	, "Int"  , 800	; fnWeight [FW_EXTRABOLD]
	, "UInt", 0		; fdwItalic
	, "UInt", 0		; fdwUnderline
	, "UInt", 0		; fdwStrikeout
	, "UInt", 1		; fdwCharset [0=ANSI, 1=Default, 255=OEM]
	, "UInt", 4		; fdwOutputPrecision [4=OUT_TT_PRECIS, 7=OUT_TT_ONLY_PRECIS]
	, "UInt", 0		; fdwClipPrecision
	, "UInt", 4		; fdwQuality [ANTIALIASED_QUALITY]
	, "UInt", 0		; fdwPitchAndFamily
;	, "Str"  , "Arial Unicode MS" ; Either this or next best Unicode font
	, "Str"  , hintsFont ; Either this or next best Unicode font
	, "Ptr")
hOFont := DllCall("gdi32\SelectObject", "Ptr", hDC, "Ptr", hFont, "Ptr")
GuiControl,, Layout, % "|" LayoutList
GuiControl, ChooseString, Layout, % Layout
HintsInit := 1
GoSub UpdateLayout
If Debug
	OutputDebug, % A_ThisLabel ": " A_TickCount-�
Return
;================================================================
WndProc(hwnd, msg, wP, lP)
{
Global
If (msg=0xF)	; WM_PAINT
	SetTimer, UpdateOpenGL, -0
Return DllCall("user32\CallWindowProc" AW, "Ptr", hWP, "Ptr", hwnd, "UInt", msg, "Ptr", wP, "Ptr", lP)
}
;================================================================
PlayTone(wParam, lParam)
{
Global
Local cTone, sc
sc := (lParam>>16)&0xFF
If (Key%sc% && !Key%sc%down)
	{
	cTone := (Key%sc% + 52) + ((Octave - 1) * 12)
	GuiControl, Focus, Focusbutton
	DllCall(midiOutShortMsg, "Ptr", hMidiOut, "UInt", (Velocity<<16) + (cTone<<8) + NoteOn)
	KeyIndex := Key%sc%
	Key%sc%down := KeyI%KeyIndex%down := 1
	SetTimer, UpdateOpenGL, -0
	Return 0
	}
Return
}
;================================================================
ReleaseTone(wParam, lParam)
{
Global
Local cTone, sc
sc := (lParam>>16)&0xFF
If Key%sc%down
	{
	KeyIndex := Key%sc%
	Key%sc%down := KeyI%KeyIndex%down := 0
	cTone := (Key%sc% + 52) + ((Octave - 1) * 12)
;	DllCall(midiOutShortMsg, "Ptr", hMidiOut, "UInt", (Velocity<<16) + (cTone<<8) + NoteOff)
	DllCall(midiOutShortMsg, "Ptr", hMidiOut, "UInt", (cTone<<8) + NoteOff)	; no release velocity
	SetTimer, UpdateOpenGL, -0
	Return 0
	}
Return
}
;================================================================
CalClick(pos)
{
Global NumKeys, KbdCW, KbdCH, KeyWW, KeyBW, MYM
Key := 0
MouseX := (pos & 0xFFFF) / KbdCW * 100
MouseY := (pos >> 16) / KbdCH * 10
CalcXPos := KeyWW - (KeyBW / 2)
Index := 1
Loop, % NumKeys
	{
	If Index in 2,4,6,9,11
		{
		If (MouseX < (CalcXPos + KeyBW)) && (MouseX > CalcXPos) && (MouseY < MYM)
			{
			Key := A_Index
			Break
			}
		CalcXPos := CalcXPos + KeyWW
		}
	If Index in 6,11
		CalcXPos := CalcXPos + KeyWW
	Index := Index > 11 ? 1 : Index+1
	}
If !Key
	{
	CalcXPos := 0
	Index := 1
	Loop, % NumKeys
		{
		If Index in 1,3,5,7,8,10,12
			{
			If (MouseX > CalcXPos) && (MouseX < (CalcXPos + KeyWW))
				{
				Key := A_Index
				Break
				}
			CalcXPos := CalcXPos + KeyWW
			}
		Index := Index > 11 ? 1 : Index+1
		}
	}
Return Key
}
;================================================================
MouseMove(wP, lP, msg, hwnd)
{
Global
Ldrag := wP&0x1, Mdrag := wP&0x10, Rdrag := wP&0x2, Sdrag := wP&0x4, Cdrag := wP&0x8
If (hwnd=hKeys)
	{
	k := CalClick(lP)
	If (k=MouseClick)
		{
		Sleep, 1
		Return
		}
	If Ldrag
		{
		MouseClick := k
		ReleaseTone(0, DnKey)
		PlayTone(0, DnKey := MKey%MouseClick%<<16)
		}
	}
}
;================================================================
MouseClick(wParam, lParam, msg, handle)
{
Global
If (DrawMode && handle=hKeys)
	{
	MouseClick := CalClick(lParam)
	PlayTone(0, DnKey := MKey%MouseClick%<<16)
	}
Return
}
;================================================================
MouseRelease(wParam, lParam, msg, handle)
{
Global
ReleaseTone(0, DnKey)
MouseClick := 0
Return
}
;================================================================
MWheel(wP, lP)
{
Global hMain, hVol, hVel, Volume, Velocity
Critical
x := lP&0xFFFF, y := (lP>>16)&0xFFFF
SetFormat, IntegerFast, H
hHover := DllCall("user32\WindowFromPoint", "Int", x, "Int", y, "Ptr")
hF := DllCall("user32\GetFocus", "Ptr")
SetFormat, IntegerFast, D
dt := (wP>>16)&0xFFFF > 0x7FFF ? "+1" : "+-1"
If (hHover=hVol)
	{
	GuiControl,, Vol, % dt
	GoSub ChooseVol
	Return 0
	}
Else If (hHover=hVel)
	{
	GuiControl,, Vel, % dt
	GoSub ChooseVel
	Return 0
	}
Else
	{
;	1=GA_PARENT, 2=GA_ROOT, 3=GA_ROOTOWNER
	hA := DllCall("user32\GetAncestor", "Ptr", hHover, "UInt", 1, "Ptr")
	If (hA != hMain)
		Return	; if not zero, it will send wheel msg to focused control regardless of what is hovered
	If (hF != hHover)
		DllCall("user32\SetFocus", "Ptr", hHover)
	SendMessage, 0x20A, %wP%, %lP%,, ahk_id %hHover%
	Return 0
	}
}
;================================================================
HideKeys(wP, lP, msg, hwnd)
{
Global
SetFormat, Integer, H
hwnd+=0
SetFormat, Integer, D
If hwnd not in %hKeys%,%hPreview%
	Return
SendMessage, 0xA5, %wP%, %lP%,, ahk_id %hwnd%
If (hwnd=hKeys)
	SetTimer, 2GuiClose, -0
Else If (hwnd=hPreview)
	SetTimer, 3GuiClose, -0
Return 0
}
;================================================================
GetWndPos(hwnd, ByRef x, ByRef y, ByRef w, ByRef h)
{
Static sz:=44, WP
If !WinExist("ahk_id " hwnd)
	Return
VarSetCapacity(WP, sz, 0)	; WINDOWPLACEMENT struct
NumPut(sz, WP, 0, "UInt")
If !DllCall("user32\GetWindowPlacement", "Ptr", hwnd, "Ptr", &WP)
	Return
c := NumGet(WP, 8, "UInt")		; 6=SW_MINIMIZE, 1=SW_SHOWNORMAL, 0=SW_HIDE
x := NumGet(WP, 28, "Int"), y := NumGet(WP, 32, "Int")
w := NumGet(WP, 36, "Int")-x, h := NumGet(WP, 40, "Int")-y
Return c	; is this another fucked up API or is it AHK ? why don't I get SW_HIDE for a hidden GUI ???
}
;================================================================
SetWndPos(hwnd, x, y, w, h, show:=1)
{
Static sz:=44, WP
If !WinExist("ahk_id " hwnd)
	Return
VarSetCapacity(WP, sz, 0)	; WINDOWPLACEMENT struct
NumPut(sz, WP, 0, "UInt"), NumPut(show ? 1 : 0, WP, 8, "UInt")	; 1=SW_SHOWNORMAL
NumPut(x, WP, 28, "Int"), NumPut(y, WP, 32, "Int")
NumPut(w+x, WP, 36, "Int"), NumPut(h+y, WP, 40, "Int")
Return DllCall("user32\SetWindowPlacement", "Ptr", hwnd, "Ptr", &WP)
}
;================================================================
GetClientSz(hwnd, ByRef w, ByRef h)
{
Static
VarSetCapacity(RECT, 16, 0)
DllCall("user32\GetClientRect", "Ptr", hwnd, "Ptr", &RECT)
w := NumGet(RECT, 8, "UInt"), h := NumGet(RECT, 12, "UInt")
}
;================================================================
#include %A_ScriptDir%\LayoutList.ahk
