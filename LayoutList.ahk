﻿/* EXAMPLE
HotkeysLine1 := "56,1E,2C,1F,2D,20,2E,2F,22,30,23,31,32,25,33,26,34,27,35"
HotkeysLine2 := "10,03,11,04,12,13,06,14,07,15,08,16,17,0A,18,0B,19,1A,0D,1B"
Hotkeys := HotkeysLine1 "," HotkeysLine2
LayoutList(Llist)
BuildDemoKbd(currHKL)
FillDemoKbd(currHKL)
return
*/
3GuiClose:
Gui, 3:Hide
Return

LayoutList(ByRef list)
{
Global currKLID, currHKL, currName, instHKL
key := "System\CurrentControlSet\Control\Keyboard Layouts"
DllCall("GetKeyboardLayoutName", "Str", currKLID:="00000000")
currHKL := DllCall("GetKeyboardLayout", "UInt", 0, "Ptr")
if count := DllCall("GetKeyboardLayoutList", "UInt", 0, "UPtr", 0)
	{
	; This may not retrieve all installed layouts !
	VarSetCapacity(hklbuf, count*A_PtrSize, 0)
	if count := DllCall("GetKeyboardLayoutList", "UInt", count, "UPtr", &hklbuf)
		Loop, %count%
			instHKL .= NumGet(hklbuf, A_PtrSize*(A_Index-1), "Ptr") ","
	StringTrimRight, instHKL, instHKL, 1
	}
list := ""
Loop, HKLM, %key%, 2
	{
	KLID := A_LoopRegName
	RegRead, f, HKLM, %key%\%KLID%, Layout file
	RegRead, name, HKLM, %key%\%KLID%, Layout text
	If (KLID=currKLID)
		currName := name
	SplitPath, f, n, d
	If (n && !d)
		f := A_WinDir "\System32\" n
	If (!FileExist(f) OR !n)
		Continue
	if !HKL := DllCall("user32\LoadKeyboardLayout" AW
		, "Str", KLID
		, "UInt", 0x80 ; KLF_NOTELLSHELL
		, "Ptr")
		HKL := currHKL
	If HKL not in %instHKL%
		DllCall("user32\UnloadKeyboardLayout", "Ptr", HKL)
	list .= name "  |" HKL  "|" KLID "`n"
	}
If !DllCall("ActivateKeyboardLayout", "Ptr", currHKL, "UInt", 0)
	OutputDebug, Failed to activate old keyboard layout
StringTrimRight, list, list, 1
Sort, list, CL
Loop, Parse, list, `n, `r
	{
	StringSplit, v, A_LoopField, |
	listN .= v1 "|"
	}
StringTrimRight, listN, listN, 1
Return listN
}
;================================================================
GetSelKLID(list, name)
{
name=%name%
Loop, Parse, list, `n, `r
	If InStr(A_LoopField, name "  |")=1
	{
	StringSplit, v, A_LoopField, |, %A_Space%
	Return v3
	}
}
;================================================================
BuildDemoKbd(HKL)
{
Global
Static s, fs, idx, i, x, y, w, h, t, col ;, e1, e2, e3 ; Can somebody explain WHAT THE FUCK IS GOING ON ???
KeyMap := "
(
29 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E|20
0F|12 10 11 12 13 14 15 16 17 18 19 1A 1B 00|01 1C|11|20
3A|13 1E 1F 20 21 22 23 24 25 26 27 28 2B
2A|11 56 2C 2D 2E 2F 30 31 32 33 34 35 36|23
1D|12 5B 38|12 39|60 38|12 5C 5D 1D|12
)"
s := 32 ; square size
fs := 15 ; font size
Gui, 3:+ToolWindow -SysMenu +E0x40000 +hwndhPreview
Gui, 3:Margin, 1, 1
Gui, 3:Color, White, White
Gui, 3:Font, s%fs% Bold, Segoe UI Symbol
Gui, 3:Font, s%fs% Bold, Arial
Gui, 3:Font, s%fs% Bold, Arial Unicode MS
idx := 1
Loop, Parse, KeyMap, `n, `r
	{
	i := A_Index, x:=1, y:=(s+1)*(i-1)+1
	Loop, Parse, A_LoopField, %A_Space%
		{
		w:=h:=s, e2:=e3:="10"
		StringSplit, e, A_LoopField, |	; e1=scancode
		StringLeft, t, e2, 1
		StringTrimLeft, e2, e2, 1
		w := Ceil(s*t+e2*(s/4))+t-((A_Index=1 && e2) || t>5 ? 0 : 1)
		StringLeft, t, e3, 1
		StringTrimLeft, e3, e3, 1
		h := Ceil(s*t+e3*(s/4))+t-1
		if e1=00
			{
			x+=w+1
			continue
			}
		col := InStr(Hotkeys, e1) ? i>2 ? "Blue" : "Red" : "E0E0E0"
		Gui, 3:Add, Text, x%x% y%y% w%w% h%h% +0x280 Center c%col% Border vdemo%idx%
;			, % GetChar("0x" e1, HKL)
		x+=w+1
		idx++
		}
	}
}
;================================================================
GetChar(SC, HKL)
{
Static ks, buf, sz:=10, spec := "
	(LTrim
	8=Backspace=0x2190
	9=Tab=0x21B9
	13=Enter=0x21B5
	16=Shift=0x21E7
	17=Ctrl
	18=Alt
	20=Caps
	32=Space
	241=LWin=0x229E
	234=RWin=0x229E
	249=Apps=0x2630
	160=LShift=0x21E7
	161=RShift=0x21E7
	162=LCtrl
	163=RCtrl
	164=Alt
	165=AltGr
	)"
VK := DllCall("MapVirtualKeyEx"
	, "UInt", SC
	, "UInt", 1						; MAPVK_VSC_TO_VK=1
	, "Ptr" , HKL
	, "UInt") & 0xFFFF
If VK in 8,9,13,16,17,18,20,32,241,234,249,160,161,162,163,164,165
	{
	Loop, Parse, spec, `n, `r
		If InStr(A_LoopField, VK "=")
			{
			StringSplit, s, A_LoopField, =
			n := s3 ? Chr(s3) : s2 ? s2 : ""
			Return n
			}
	Return ""
	}
VarSetCapacity(ks, 256, 0), VarSetCapacity(buf, 2*sz, 0)
n := DllCall("ToUnicodeEx"
	, "UInt", VK
	, "UInt", SC
	, "Ptr"  , &ks
	, "Ptr"  , &buf
	, "Int"  , sz
	, "UInt", 0
	, "Ptr"  , HKL
	, "Int")
If n<0
	n := DllCall("ToUnicodeEx"
		, "UInt", VK
		, "UInt", SC
		, "Ptr"  , &ks
		, "Ptr"  , &buf
		, "Int"  , sz
		, "UInt", 0
		, "Ptr"  , HKL
		, "Int")
c := StrGet(&buf, 1, "UTF-16")
StringUpper, c, c
Return, c
}
;================================================================
FillDemoKbd(HKL)
{
Static
Global KeyMap, Layout
idx := 1
Loop, Parse, KeyMap, `n, `r
	Loop, Parse, A_LoopField, %A_Space%
		{
		StringSplit, e, A_LoopField, |	; e1=scancode
		if e1=00
			continue
		GuiControl, 3:, demo%idx%, % GetChar("0x" e1, HKL)
		idx++
		}
Gui, 3:Show,, Preview layout: %Layout%
}
