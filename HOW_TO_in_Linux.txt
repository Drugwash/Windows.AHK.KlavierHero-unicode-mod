Run this in a Terminal window:

fluidsynth --server --audio-driver=alsa -o audio.alsa.device=hw:0 /usr/share/sounds/sf2/FluidR3_GM.sf2

Before this you should read Ted's Linux MIDI Guide.html:
http://www.tedfelix.com/linux/linux-midi.html

